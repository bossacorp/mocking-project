package bossacorp.tdd.agencia.test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import bossacorp.tdd.agencia.Vuelo;

public class BusquedaVueloTest {

	@Test
	public void searchFlightTest() {
		
		//arrange
		String origen, destino;
		origen = "Guadalajara";
		destino = "Zapopan";
		
		Vuelo v = new Vuelo();
		
		//Act
		boolean resultadoEsperado = false;
		
		//Assert
		assertEquals(resultadoEsperado, v.searchFlight(origen,destino));
		
	}
	
	@Test
	public void searchFlightTestWithStub(){
		//arrange
		Vuelo vMock = mock(Vuelo.class);
		
		String origen, destino;
		origen = "Guadalajara";
		destino = "Zapopan";
		
		when(vMock.searchFlight(origen, destino)).thenReturn(true);
		
		assertEquals(true,vMock.searchFlight(origen, destino));
		//vMock.doneSomething();
		
		verify(vMock).searchFlight(origen, destino);
		//verify(vMock).doneSomething();
	}

}
